# heatpump mock

This project contains a simulation of a HeatPump which can be attached to HomeAssistant.

This
* is a Modbus TCP server
* simulates Nibe SMOS40 device (s line)


CoilGroup:

"s1": ClimateCoilGroup(
name="Climate System S1",
current=30027,
setpoint_heat=40207,
setpoint_cool=40989,
mixing_valve_state=31034,
active_accessory=None,
use_room_sensor=40203,
),


coils: smos40:

"30027": {
"title": "Roomsensor 1-1",
"factor": 10,
"unit": "\u00b0C",
"size": "s16",
"name": "roomsensor-1-1-30027"
},

"40207": {
"title": "Room sensor set point value climate system 1",
"factor": 10,
"unit": "\u00b0C",
"size": "s16",
"min": 50.0,
"max": 300.0,
"default": 200.0,
"name": "room-sensor-set-point-value-climate-system-1-40207",
"write": true
},

"40989": {
"title": "Room sensor set point value climate system 1, cooling",
"factor": 10,
"unit": "\u00b0C",
"size": "s16",
"min": 50.0,
"max": 350.0,
"default": 250.0,
"name": "room-sensor-set-point-value-climate-system-1-cooling-40989",
"write": true
},

"31034": {
"title": "Oper. mode shunt climate system 1",
"factor": 1,
"size": "u8",
"name": "oper-mode-shunt-climate-system-1-31034"
},

"40203": {
"title": "Use room sensor climate system 1",
"factor": 1,
"size": "u8",
"min": 0.0,
"max": 1.0,
"default": 0.0,
"name": "use-room-sensor-climate-system-1-40203",
"write": true
},



Logic for calls on modbus: venv/lib/python3.10/site-packages/nibe/connection/modbus.py
venv/lib/python3.10/site-packages/nibe/connection/modbus.py