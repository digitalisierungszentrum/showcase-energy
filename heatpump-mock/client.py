import asyncio

import connio
from umodbus.client import tcp


async def send_message_tcp(adu, reader, writer):
    """Send ADU over asyncio reader/writer and return parsed response.

    :param adu: Request ADU.
    :param reader: an async stream reader (ex: asyncio.StreamReader)
    :param writer: an async stream writer (ex: asyncio.StreamWriter)
    :return: Parsed response from server.

    TAKEN FROM https://pypi.org/project/async-modbus/, GPL 3
    """
    await writer.write(adu)

    exception_adu_size = 9
    response_error_adu = await reader.readexactly(exception_adu_size)
    tcp.raise_for_exception_adu(response_error_adu)

    expected_response_size = (
            tcp.expected_response_pdu_size_from_request_pdu(adu[7:]) + 7
    )
    response_remainder = await reader.readexactly(
        expected_response_size - exception_adu_size
    )

    return tcp.parse_response_adu(response_error_adu + response_remainder, adu)


async def main():
    stream = connio.connection_for_url("tcp://localhost:5020", concurrency="async")
    request = tcp.read_input_registers(1, 0x01, 1)
    res = await send_message_tcp(request, stream, stream)
    print(res)
    request = tcp.write_single_register(1, 0x01, 5)
    res = await send_message_tcp(request, stream, stream)
    print(res)
    request = tcp.read_input_registers(1, 0x01, 1)
    res = await send_message_tcp(request, stream, stream)
    print(res)


if __name__ == "__main__":
    asyncio.run(main(), debug=True)
