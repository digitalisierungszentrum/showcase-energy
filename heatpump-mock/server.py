#!/usr/bin/env python3
"""
    Original from https://github.com/pymodbus-dev/pymodbus/blob/dev/examples/server_async.py @ 2022-02-24
"""
import asyncio
import logging

from pymodbus.datastore import ModbusServerContext, ModbusSlaveContext, ModbusSparseDataBlock
from pymodbus.device import ModbusDeviceIdentification
from pymodbus.server import StartAsyncTcpServer
from pymodbus.version import version

_logger = logging.getLogger()
logging.basicConfig(level=logging.DEBUG)

bind_address = ""
bind_port = 5060


async def run_async_server():
    """Run server."""
    txt = f"### start ASYNC server, listening on {bind_address}:{bind_port}"
    _logger.info(txt)
    address = (bind_address, bind_port)

    datablock = ModbusSparseDataBlock({0x00: 0, 0x05: 1})  # TODO initial data
    slave_context = ModbusSlaveContext(
        di=datablock, co=datablock, hr=datablock, ir=datablock, unit=1
    )

    context = ModbusServerContext(slaves=slave_context, single=True)

    identity = ModbusDeviceIdentification(
        info_name={
            "VendorName": "Pymodbus",
            "ProductCode": "PM",
            "VendorUrl": "https://github.com/pymodbus-dev/pymodbus/",
            "ProductName": "Pymodbus Server",
            "ModelName": "Pymodbus Server",
            "MajorMinorRevision": version.short(),
        }
    )

    server = await StartAsyncTcpServer(
        context=context,  # Data storage
        identity=identity,  # server identify
        # TBD host=
        # TBD port=
        address=address,  # listen address
        # custom_functions=[],  # allow custom handling
        # framer=args.framer,  # The framer strategy to use
        # handler=None,  # handler for each session
        allow_reuse_address=True,  # allow the reuse of an address
        # ignore_missing_slaves=True,  # ignore request to a missing slave
        # broadcast_enable=False,  # treat unit_id 0 as broadcast address,
        # timeout=1,  # waiting time for request to complete
        # TBD strict=True,  # use strict timing, t1.5 for Modbus RTU
        # defer_start=False,  # Only define server do not activate
    )
    return server


if __name__ == "__main__":
    asyncio.run(run_async_server(), debug=True)
